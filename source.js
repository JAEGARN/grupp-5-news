
// JavaScript Document
var app = angular.module('theNews', ['ngRoute', "firebase", 'ui.bootstrap']);

app.config(["$routeProvider", function($routeProvider){
    $routeProvider.when("/", {
    	redirectTo: "/articles"
    });

    $routeProvider.when("/articles", {
    	templateUrl: "showArticles.html"
    });
    
    $routeProvider.when("/login", {
    	templateUrl: "logIn.html"
    });

    $routeProvider.when("/addArt", {
    	templateUrl: "newArticle.html"
    });

	$routeProvider.when("/showArt/:aId", {
		templateUrl: "showWholeArticle.html",
		controller: "showArtCtrl"
	});

	 
	
}]);

app.filter('reverser', function(){
	return function(zeArray){
				
		if(!angular.isArray(zeArray)){
			if(typeof zeArray === 'object'){
				var tempArray = []
				for(var i in zeArray){
					zeArray[i].id = i;
					tempArray.push(zeArray[i]);
				}
				return tempArray.slice().reverse();
			}
			return zeArray;			
		}
		return zeArray.slice().reverse();
	};
});

app.service('newsService', function(){
	var myDataRef = new Firebase('https://blistering-fire-7793.firebaseio.com/');
	var wichArticle;
	var whichSite = "Hem";
	var firebaseData;
	var comRef;
	var pickedCategory;
	var menuItems = [
		{menuTitle: "Hem", menuRoute: "articles"},
		{menuTitle: "Logga in", menuRoute: "login"},
	];
	var categories = ['Agillity', 'Strength', 'Intelligence', 'Alla Hjältar'];
	return {
		"menuItems": menuItems,
		"firebaseData": firebaseData,
		"whichSite": whichSite,
		"wichArticle": wichArticle,
		"myDataRef": myDataRef,
		"comRef": comRef,
		"pickedCategory": pickedCategory,
		"categories": categories
	};
	
});

app.controller('logInCtrl', ['$scope', 'newsService', '$location', '$firebaseAuth', function($scope, ns, $location, $firebaseAuth){
	$scope.submitForm = function (ev) {
		if ($scope.logInForm.$valid) {
			var auth = $firebaseAuth(ns.myDataRef);
				
			auth.$authWithPassword({
        		email: $scope.username,
        		password: $scope.password
      		}).then(function(user) {
        		ns.menuItems[1] = {menuTitle: "Lägg till Artikel", menuRoute: "addArt"};
				ns.menuItems[2] = {menuTitle: "Logga ut", menuRoute: "logOut"};
				$location.path("/");
      		}, function(error) {
        		alert(error);
        	});
		} else {
			ev.preventDefault();			
		}
	}
	
}]);

app.controller('showArtCtrl', ['$scope', 'newsService', '$location', '$routeParams', '$firebaseObject', function($scope, ns, $location, $routeParams, $firebaseObject){
	$scope.id = $routeParams.aId;
	var articleDataRef = new Firebase('https://blistering-fire-7793.firebaseio.com/'+$scope.id);
		
	$scope.article = $firebaseObject(articleDataRef);
	$scope.isLoggedIn = ns.myDataRef.getAuth();
	
	$scope.submitCommentForm = function (ev) {
		if ($scope.commentForm.$valid) {
			var comRef = ns.myDataRef.child($scope.article.$id);
			comRef.child('comments').push({name: $scope.form.name, text: $scope.form.text});
			var frm = document.getElementsByName('commentForm')[0];
			frm.reset();
		} else 
		
			ev.preventDefault();			
		}


	$scope.delContent = function(art){
		if(ns.myDataRef.getAuth()){
			ns.myDataRef.child(art.$id).remove();
			$location.path("/");
		}else {
			alert("You have to be logged in");
		}
	}

	$scope.editContent = function(art) {
		$scope.formUptitle = art.artTitle;
		$scope.formUptext = art.artText;
	}

	function el(id){return document.getElementById(id);}

	function readImage() {
    if ( this.files && this.files[0] ) {
        var FR= new FileReader();
        FR.onload = function(e) {
			$scope.formUpimage = e.target.result;
        };       
        FR.readAsDataURL( this.files[0] );
    	}
	}

	el("exampleInputFile").addEventListener("change", readImage, false);

	$scope.updateArticle = function(art) {
		
		
		if($scope.formUpimage !== undefined){
			
			articleDataRef.update({artTitle: $scope.formUptitle, imgSrc: $scope.formUpimage , artText: $scope.formUptext});	
		}else {
			
			articleDataRef.update({artTitle: $scope.formUptitle, artText: $scope.formUptext});
		}
		
	}

	$scope.delComment = function(com){
		ns.myDataRef.child($scope.article.$id).child('comments').child(com.id).remove();
	}

}]);

app.controller('menuCtrl', ['$scope', 'newsService', '$location', function($scope, ns, $location){
	$scope.theMenuItems = ns.menuItems;	

	$scope.whichMenuItem = function(b) {
		if(b.menuRoute === "logOut"){
			ns.isLoggedIn = false;
			ns.myDataRef.unauth();
			ns.menuItems[1] = {menuTitle: "Logga in", menuRoute: "login"};
			ns.menuItems.splice(2, 1);
			$location.path("/");
		}else{
			$location.path("/"+b.menuRoute);	
		}
		
	}
	
}]);

app.controller('contentCtrl', ['$scope','newsService','$location', '$firebaseArray', '$routeParams', function($scope, ns, $location, $firebaseArray, $routeParams) {
	$scope.theArticles = $firebaseArray(ns.myDataRef);
	//var arts = element.all(by.repeater('a in theArticles'));
	
	ns.firebaseData = $scope.theArticles;
	$scope.categories = ns.categories;

	// Filtret catFilter fungerar om man hårdkodar kategori
	// i pickedCategory på rad 160.
	// Tanken är att function rad 169 skall ändra värdet på rad 160
	//men det verkar inte funka. Weird.
	ns.pickedCategory = "Alla Hjältar";
	$scope.pickedCategory2 = "Alla Hjältar";
	
	//$scope.theArticles = ns.firebaseData;
 	$scope.whichArt = function(aId){
		ns.wichArticle = aId;
		$location.path('/showArt/' + aId);
	}
	$scope.setCat = function(cat){
		ns.pickedCategory = cat;
		$scope.pickedCategory2 = ns.pickedCategory;
		$location.path("/");
	}
	$scope.catFilter = function (art) {
		if(ns.pickedCategory === "Alla Hjältar"){
			return true;
		}	else if(ns.pickedCategory === art.category){
			return true;
		} else {
			return false;
		}
	}
	$scope.searchFilter = function(art){
		if($scope.a.str && $scope.a.str.length > 0){
			if(art.artTitle.toLowerCase().indexOf($scope.a.str.toLowerCase()) !== -1){
			return true;	
			}
		}else {
			return true;
		}

		return false;
	}

}]);



app.controller('bodyCtrl', ['$scope', 'newsService', function ($scope, ns) {
	$scope.a = {str: ""};

	if(ns.myDataRef.getAuth()){
		ns.menuItems[1] = {menuTitle: "Lägg till Artikel", menuRoute: "addArt"};
		ns.menuItems[2] = {menuTitle: "Logga ut", menuRoute: "logOut"};
	}
    
}]);

app.controller('formCtrl', ['$scope', 'newsService', '$location', function($scope, ns, $location) {
	$scope.options = ns.categories;
	$scope.category = $scope.options[0];

	$scope.createFilter = function(option){
		if(option != "Alla Hjältar"){
			return true;
		}
	}
	function el(id){return document.getElementById(id);} // Get elem by ID

	function readImage() {
    if ( this.files && this.files[0] ) {
        var FR= new FileReader();
        FR.onload = function(e) {
            $scope.imageCode = e.target.result;
        };       
        FR.readAsDataURL( this.files[0] );
    	}
	}

	el("exampleInputFile").addEventListener("change", readImage, false);
	
	$scope.submitForm = function (ev) {
		if ($scope.myForm.$valid) {
			if(ns.myDataRef.getAuth()){
				$scope.newRef = ns.myDataRef.push({artTitle: $scope.form.title, imgSrc: $scope.imageCode, artText: $scope.form.text, category: $scope.category});
				//ns.comRef = $scope.newRef.toString();
				$location.path("/");	
			}else {
				alert("You are NOT logged In");
			}					
			ev.preventDefault();			
		}
	}	
}]);


 



